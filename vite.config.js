import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

export default defineConfig({
    // If process.env.CI_PROJECT_NAME exists, use that as the build base
    base: process.env.CI_PROJECT_NAME ? `/${process.env.CI_PROJECT_NAME}/` : "/",
    plugins: [vue()],
    server: {
        port: 5000,
    }
});
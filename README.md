# Vue Boilerplate
Minimal Vue boilerplate set up for SCSS and GitLab Pages.

[![Vue Icon](https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Vue.js_Logo_2.svg/32px-Vue.js_Logo_2.svg.png)](https://vuejs.org/)  [![SASS Icon](https://sass-lang.com/favicon.ico)](https://sass-lang.com/)  [![Gitlab Pages Icon](https://docs.gitlab.com/favicon.ico?v=2)](https://sass-lang.com/)

## Clone project
```
git clone https://gitlab.com/tiffeichelberger/vue-boilerplate
```

## Install dependencies
```
npm install
```

## Build project
```
npm run build
```

## Start development server
```
npm run start
```

[http://localhost:5000](http://localhost:5000) to view it locally.

[https://tiffeichelberger.gitlab.io/vue-boilerplate/](https://tiffeichelberger.gitlab.io/vue-boilerplate/) to view a demo.